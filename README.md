# projects_datasets

Ce dossier regroupe plusieurs corpus textuels de petite tailles annotées, utilisés au sein du LISN.

## Tâches

Les différents corpus sont soit associés à des tâches de Natural Language Understanding (NLU) ou de Recherche d'Information (RI).

## Corpus NLU

| Corpus | Type d'annotations (tâche) | Domaine | Formats | Langues |
| -- | -- | -- | -- | -- |
| DiBISO | Concepts (slot filling), Intentions | Bibliothèques universitaires | `.tsv` `.json` `.yaml` | FR |
| Humane.AI | Concepts (slot filling), Intentions | Tourisme | `.tsv` | EN, FR |
| MEDIA | Concepts (slot filling), Intentions (travail en cours) | Tourisme, hôtellerie | `.tsv`| FR |
| Ubib | Concepts (slot filling), Intentions | Bibliothèques universitaires | `.tsv` `.json` `.yaml` | FR, (EN) |


## Corpus RI

| Corpus | Type d'annotations (tâche) | Domaine | Formats | Langues |
| -- | -- | -- | -- | -- |
| DiBISO | Recherche d'information / Retrieval | Renseignements sur les bibliothèque de l'Université de Paris-Saclay | `.tsv`, `.json` | FR |


## Détails

Plus d'informations sont disponibles dans chaque sous dossier.

## Corpus supplémentaires

```/people/alavoine/Documents/datasets_ri``` pour des corpus orientés RI


```/people/alavoine/Documents/datasets_nlu``` pour des corpus orientés NLU
