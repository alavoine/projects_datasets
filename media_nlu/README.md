### Corpus MEDIA

Ce dossier contient le corpus MEDIA (tourisme - réservation d'hôtel) annoté en concepts (tâche de slot filling) originellement.
Les versions textuelles de MEDIA ont été transcrites depuis des fichiers audio.
Une version en intention, exclusive, est aussi présentée.

# Source
Cette version de MEDIA a été fournie par une source interne du LISN.

# Format
- txt (original)
- tsv (original, annoté en intention)

# Version
Plusieurs versions d'annotation de ce corpus sont disponibles.
Le nom/numéro de version correspondant à "original" n'est pas identifié, il s'agit d'une version "relax" des concepts.

# Dossiers
- non_annote_en_intentions : Version de MEDIA annoté en concepts uniquement. Comprend la version originale de MEDIA, avec des concepts éventuellement corrigés. Comprend aussi la version [Brainspeech](https://github.com/speechbrain/speechbrain/blob/develop/recipes/MEDIA/) de MEDIA, où les transcriptions ont été retravaillées.
- annote_en_intentions : Version de MEDIA annoté en intentions et concepts (+ ids). Comprend la version originale et la version Brainspeech.

## Autre

### Caractéristiques

|Caractéristique|Valeur |
|---|--- |
|Taille du lot d'entrainement|12916 énoncés |
|Taille du lot de validation|1259 énoncés  |
|Taille du lot d'évaluation|3518 énoncés |
|Nombre d'étiquettes de concepts|76 étiquettes |
|Nombre d'étiquettes d'intentions|11 étiquettes |

### Distribution des étiquettes d'intentions

Dans la version originale :

|Intention|# train|# dev|# test |
|---|---|---|--- |
|annulation|32|1|15|48 |
|incomprehension|273|30|94|397 |
|marqueur_discursif|282|40|113|435 |
|modification|115|10|31|156 |
|remercier|713|100|200|1013 |
|renseignements|1611|159|401|2171 |
|reponse_affirmative|4325|419|1190|5934 |
|reponse_indecise|37|5|9|51 |
|reponse_negative|1315|88|344|1747 |
|reservation|5437|522|1410|7368 |
|saluer|717|101|206|1024 |

## Plus de détails

[Document overleaf](https://fr.overleaf.com/read/zjxmjdnkdpbb)