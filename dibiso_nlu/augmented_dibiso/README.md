Patterns obtained with a slightly modified version of : https://github.com/HugoBoulanger/Pattern-Filling-Generation

Fold 5 was used as it was the fold with the worst mean F1 score on multiple runs
Train and test were augmented with pattern filling generation. Duplicates were removed. Train was cut into train and dev set (10%).