from src.extraction import *
from src.full_extraction import *
from src.generator import *
from src.patterns import *
from config import GenerationConfig
from pathlib import Path, PosixPath


import argparse


def run_generation(args):
    """
    Generation function using the previous generation functions
    """

    cfg = GenerationConfig(args.dir / 'config.yml')
    name = str(cfg.generation.save_dir / f'generated_{str(cfg.generation.dataset).split("/")[-1].split(".")[0]}.txt')
    if os.path.exists(name):
        print(f"\tFile {name} already exists. Please delete it or rename it before launching this script.")
        exit()

    if cfg.generation.do_extraction:
        if not os.path.exists(args.dir / 'extracted'):
            os.mkdir(str(args.dir / 'extracted'))

        extract(str(args.dir / 'extracted'), cfg)

    generator = Algo1(str(args.dir/f'extracted/patterns_{ cfg.generation.ver }.txt'), str(args.dir/f'extracted/lists_{cfg.generation.ver}.txt'), cfg)

    generated = generator.generate_corpus(size=cfg.generation.size)
    f = open(name, 'w+')
    f.writelines(generated)
    f.close()
    print(f'\tSuccessfully generated :  {name}')



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("dir", type=str, help="str. Directory where a config file is found")
    args = parser.parse_args()

    args.dir = Path(args.dir)
    run_generation(args)