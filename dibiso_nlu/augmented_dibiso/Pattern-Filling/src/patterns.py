import os


def read_patterns(pattern_file):
    """
    Reads the pattern files from the pattern file
    :param pattern_file: str, path of the pattern file containing the pattern files and their intent (if needed)
    :return: dictionary {intent:patterns}
    """
    f = open(pattern_file, 'r')
    lines = f.readlines()
    f.close()
    patterns = {}
    for l in lines:
        f = open(os.path.join(os.path.dirname(pattern_file), l.strip()), 'r')
        for rl in f.readlines():
            rl = rl[:-1]
            splitrl = rl.split('\t')
            if len(splitrl) == 1:
                splitrl = rl.split('   ')
            if splitrl[1] not in patterns.keys():
                patterns[splitrl[1]] = [splitrl[0]]
            else:
                patterns[splitrl[1]].append(splitrl[0])
        f.close()
    return patterns


def read_mentions(list_path):
    f = open(list_path, 'r')
    lines = f.readlines()
    f.close()
    mentions = {}
    for l in lines:
        ls = l.strip().split()
        f = open(os.path.join(os.path.dirname(list_path), ls[1]), 'r')
        mentions[ls[0]] = [l.strip() for l in f if l != '' or l != '\n']
        f.close()
    return mentions

def write_patterns(patterns, pattern_path, patterns_dir, with_intent=False):
    """

    :param patterns:
    :param pattern_path:
    :param patterns_dir:
    :param with_intent:
    :return:
    """
    if not os.path.exists(patterns_dir):
        os.mkdir(patterns_dir)

    f = open(pattern_path, 'w')
    if not with_intent:
        f.write(f'{patterns_dir}/patterns.txt\n')
        pat = open(f'{patterns_dir}/patterns.txt', 'w')
        pat.writelines(patterns['1'])
        pat.close()
    if with_intent:
        for i, pats in patterns.items():
            f.write(f'{i} {patterns_dir}/patterns_{i}.txt\n')
            pat = open(f'{patterns_dir}/patterns_{i}.txt', 'w')
            pat.writelines(patterns[i])
            pat.close()
    f.close()
