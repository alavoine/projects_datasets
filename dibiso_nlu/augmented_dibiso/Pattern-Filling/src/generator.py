import sys
import os
import re
from .patterns import read_patterns, read_mentions
import random

slotre = re.compile("\$([-a-zA-Z._0-9]+)")
btre = re.compile("<(.*)>")
etre = re.compile("</(.*)>")
doltre = re.compile("(\$.*)")


class Algo1:

    def __init__(self, pattern_path, list_path, cfg):
        self.patterns = read_patterns(pattern_path)
        self.mentions = read_mentions(list_path)
        self.rand = random.Random()
        self.rand.seed(a=cfg.generation.seed)
        self.variable = cfg.generation.variable_pattern
        self.variables_values = {}
        for var in self.variable:
            self.variables_values[var] = list(cfg.generation[var])


    def generate_corpus(self, size):
        """
        Function to generate size sentence on patterns 

        :param size: size of corpus to freshly generate
        :return:
        """
        m = 0
        for v in self.patterns.values():
            m += len(v)
        n = size // m + 1
        ns = [n for i in range(m)]
        rm = (n * m) - size
        to_rm = self.rand.sample(range(m), k=rm)
        for i in to_rm:
            ns[i] += -1
        generated = []
        i = 0
        for k, v in self.patterns.items():
            for j in range(len(v)):
                generated.extend(generate_pattern(self.mentions, v[j], ns[i], self.rand, k, self.variable, self.variables_values))
                i += 1
        return generated


def split_pattern(pattern):
    pattern = pattern.split()
    pat = "BOS"
    tags = "O"
    tag = ""
    bi = ""
    for w in pattern:
        bt = re.match(btre, w)
        et = re.match(etre, w)
        dolt = re.match(doltre, w)
        if dolt:
            pat += f" {w}"
            tags += f" {w}"
        elif et:
            tag = ""
            bi = ""
        elif bt:
            tag = bt[1]
            bi = "B"
        else:
            if tag == "":
                pat += f" {w}"
                tags += f" O"
            else:
                pat += f" {w}"
                tags += f" {bi}-{tag}"
                bi = "I"

    pat += f" EOS"
    return pat, tags


def fill_pattern(pattern, tags, fillers, intent):
    pat = pattern
    tag = tags
    for m in fillers:
        t = re.search(slotre, pat)[1]
        n = len(m.split())
        ta = f"B-{t}"
        for i in range(n-1):
            ta += f" I-{t}"
        pat = re.sub(f"\${t}", m, pat, count=1)
        tag = re.sub(f"\${t}", ta, tag, count=1)
    return f"{pat}\t{tag}\t{intent}\n"


def samples(mentions, slots, n, rand):
    combine = 1
    for slot in slots:
        combine = combine * len(mentions[slot])
    if n < combine:
        if combine > (2**63-1):
            to_generate = set()
            while len(to_generate) < n:
                to_generate.add(rand.randrange(combine))
            to_generate = list(to_generate)
        else:
            to_generate = rand.sample(range(combine), k=n)
    else:
        n_full = n // combine
        to_generate = rand.sample(range(combine), k=(n % combine))
        for i in range(n_full):
            to_generate.extend(list(range(combine)))
    return to_generate


def generate_pattern(mentions, pattern, n, rand, intent, variable, var_dict):
    pat, tags = split_pattern(pattern)
    slots = re.findall(slotre, pat)
    sub_pat = []
    sub_n = []
    sub_tags = []
    generated = []
    for slot in slots:
        if slot in variable:
            ssub_pat = []
            ssub_n = []
            ssub_tags = []
            sn = n // len(var_dict[slot]) + 1
            for var in var_dict[slot]:
                ssub_pat.append(pat.replace("$" + slot, str(var)))
                small_tag = ""
                for i, tagging in enumerate(ssub_pat[-1].split()[1:-1]):
                    if tagging.startswith("$"):
                        small_tag += tagging
                    else:
                        small_tag += "O"
                    if i != len(ssub_pat[-1].split()) - 1:
                        small_tag += " "
                ssub_tags.append(small_tag)
                ssub_n.append(sn)
            rm = (sn * len(var_dict[slot])) - n
            to_rm = rand.sample(range(len(var_dict[slot])), k=rm)
            for i in to_rm:
                ssub_n[i] += -1
            for i in range(0, len(ssub_pat)):
                sub_pat.append(ssub_pat[i])
                sub_n.append(ssub_n[i])
                sub_tags.append(ssub_tags[i])
    if len(sub_pat) == 0:
        to_generate = samples(mentions, slots, n, rand)
        for i in to_generate:
            fillers = []
            for s in slots:
                fillers.append(mentions[s][random.randint(0, len(mentions[s]) - 1)])
            generated.append(str(fill_pattern(pat, tags, fillers, intent)))
    else:
        for idx, pat in enumerate(sub_pat):
            slots = re.findall(slotre, pat)
            to_generate = samples(mentions, slots, sub_n[idx], rand)
            for i in to_generate:
                fillers = []
                for s in slots:
                    fillers.append(mentions[s][random.randint(0, len(mentions[s]) - 1)])
                generated.append(str(fill_pattern(pat, sub_tags[idx], fillers, intent)))
    return generated


