# dibiso

Corpus français DiBISO (renseignements bibliothèques) annoté en intentions et concepts (tâches de détection d'intentions et de concepts).
Issu du projet du même nom, en collaboration LISN et DiBISO (Direction des Biblithèques, de l'Information et de la Science Ouverte), pour concevoir un système interactif de questions-réponses au sujet des bibliothèques universitaires Paris-Saclay.
Ces données ont été collectés suite à une campagne d'annotation via un prototype entraîné sur le corpus ubib.

## Format

 - tsv (version corrigée)
 - json (non corrigée)

## Source

Données récupérées sous format json suite à la campagne d'annotation, corrigée par un seul annotateur.

## Autres

### Dossiers

- augmented_dibiso : Génération de données par Pattern Filling Generation
- data_for_rasa : Jeux de 5 répartitions issues des données corrigées (pour validation croisée à 5 blocs), sans ou avec données ubib (synthetic), au format yaml attendu par la librairie [RASA](https://rasa.com/) 
- dibiso_data : Données corrigées au format tsv

### Caractéristiques

| Caractéristique | Valeur |
| --- | --- |
| Nombre de questions | 471 | 
| Taille du vocabulaire | 3071 |
| Nombre d'intentions | 7 | 
| Nombre de concepts | 4 | 

### Etiquettes d'intentions

Signification :
- bot challenge : Question sur le bot / remise en question du bot
- get timetable of library : Question sur les horaires ou affluences d'une bibliothèques
- out of scope : Phrase ne rentrant pas dans les autres catégories
- search library fields : Question sur les spécialités (maths, biologie, histoire, ...) des ouvrages d'une bibliothèque universitaire
- greet : Salutations (bonjour, au revoir)
- search library address : Question sur l'adresse / la localisation d'une bibliothèque universitaire
- search libraries from field : Question pour chercher une / des bibliothèques universitaires en fonction d'une spécialité (maths, biologie, histoire, ...) d'ouvrage 

| Intention | Nombre |
| --- | --- |
| bot challenge | 26 |
| get timetable of library | 7 | 
| out of scope | 346 | 
| search library fields | 14 | 
| greet | 26 | 
| search library address | 29 | 
| search libraries from field | 23 |

### Etiquettes de concepts

Signification :
- library : Nom d'une bibliothèque universitaire
- field : Spécialité (maths, biologie, histoire, ...)
- month : Mois (dans une date)
- period_relative : Période temporelle définie (ex : les vacances de Noël)
- day_of_month : Jour du mois dans une date (ex : 15 dans "le 15 novembre")
- day_relative : Date se rapportant à un jour et définie par rapport au jour courant (ex : aujourd'hui, demain)
- week_relative : Date se rapportant à une semaine et définie par rapport à celle courante (ex : dans une semaine)
- day_of_the_week : Jour de la semaine (ex: lundi, mardi)
- date_relative : Terme permettant de situé une date (ex : prochain ou passé dans "lundi prochain" ou "le mois passé")

Certains de ces concepts sont absents du corpus dibiso, mais étaient présents dans le corpus ubib.

| Concept | Nombre |
| -- | -- |
| library | 136 | 
| field | 66 | 
| month | 0 | 
| period_relative | 1 | 
| day_of_month | 0 | 
| day_relative | 3 | 
| week_relative | 0 | 
| day_of_the_week | 0 | 
| date_relative | 0 | 

