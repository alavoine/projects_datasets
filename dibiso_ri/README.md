# dibiso

Corpus français DiBISO (renseignements bibliothèques) pour la recherche d'information.
Issu du projet du même nom, de la collaboration LISN et DiBISO (Direction des Biblithèques, de l'Information et de la Science Ouverte), pour concevoir un système interactif de questions-réponses au sujet des bibliothèques universitaires Paris-Saclay.

## Format

 - tsv 
 - json 

## Source

- Questions (queries.tsv) : Questions "out_of_scope" issue d'une première campagne d'annotation.
- Documents (documents.tsv) : Documents /passages issus du scrapping des pages du [site des bibliothèques universitaires de Paris-Saclay](https://www.bibliotheques.universite-paris-saclay.fr/) et de pages annexes. Pour en savoir plus, rendez-vous sur le [repo gitlab correspondant](https://gitlab.lisn.upsaclay.fr/alavoine/web-scrapping).
- Triplets question / passages positives / passages négatifs (triplets.tsv) : Utilisation d'un algorithme BM25 pour créer des paires positives question / passages.  Puis vérification des paires obtenues à l'aide d'un outil de classification (doccano). Les "mauvaises" paires composent les associations question / passage négative. Les "bonnes" paires composent les associations question / passage positif. Pour les questions où aucun passage positif n'était obtenu ou fortement incomplet, une correction manuelle supplémentaire a été apporté.

## Autres

### Dossiers

- documents : Comprend l'ensemble des documents sous format `.tsv` et `.json`. Pour chaque document, son identifiant (id), l'url dont il provient (url), le titre de sa page ou de son passage (title), son contenu (body) et les liens référencés (links) sont conservés. Pour la version `.tsv`, une colonne "text" comprenant le titre accolé au contenu est présent. C'est cette colonne qui servira à l'indexage des passages.

- queries : Comprends les requêtes au format `.tsv`.

- triplets : Comprend les triplets question / passage positif / passage négatif sous format `.tsv`. Les requêtes sont référencées par un identifiant et leur contenu sous format texte sont présents. Chaque requête peut être associée à un ou plusieurs documents positifs ou négatifs. Pour chaque document, son identifiant dans l'ensemble de document, le titre de la page dont ils sont issus ainsi leur contenu sous format texte sont données. Comprend aussi ces relations ne conservant que le identifiants sous format `.json` (qrel).

- dibiso_for_splade : Dossier comprenant des modifications des fichiers précédents pour permettre l'entraînement, l'évaluation d'un modèle [SPLADE](https://github.com/naver/splade).


### Caractéristiques

| Caractéristique | Valeur |
| --- | --- |
| Nombre de questions | 346 | 
| Nombre de passages | 366 |