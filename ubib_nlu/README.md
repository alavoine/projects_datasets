# ubib

Corpus ubib, fourni par la plateforme du même nom (mise en relation utilisateurs de bibliothèques universitaires / bibliothécaires en ligne via interface de chat).
Utilisé pour le projet DiBISO, issu d'une collaboration LISN et DiBISO (Direction des Biblithèques, de l'Information et de la Science Ouverte), pour concevoir un système interactif de questions-réponses au sujet des bibliothèques universitaires Paris-Saclay.
Ces données ont été annotées manuellement, augmentées par Pattern Filling Generation puis à nouveau corrigées.

## Format

 - tsv
 - yaml pour RASA

## Source

Données récupérées sous yaml pour RASA depuis le projet du prototype, corrigées par un seul annotateur.

## Autres

Le corpus est parfois appelé "dibiso_synthetic" dans les différents fichiers.
On différencie son set de train de celui de test.

### Dossiers

- rasa_data : Sets de train et test au format yaml pour RASA.
- tsv : Version tsv et formattées au propre, dossier de référence.

### Caractéristiques

| Caractéristique | Set train | Set test |
| --- | --- | --- |
| Nombre de questions | 12402 | 997 |
| Taille du vocabulaire | 15710 | 15601 |
| Nombre d'intentions | 7 | 7 | 
| Nombre de concepts | 9 | 9 |

### Etiquettes d'intentions

Signification :
- bot challenge : Question sur le bot / remise en question du bot
- get timetable of library : Question sur les horaires ou affluences d'une bibliothèques
- out of scope : Phrase ne rentrant pas dans les autres catégories
- search library fields : Question sur les spécialités (maths, biologie, histoire, ...) des ouvrages d'une bibliothèque universitaire
- greet : Salutations (bonjour, au revoir)
- search library address : Question sur l'adresse / la localisation d'une bibliothèque universitaire
- search libraries from field : Question pour chercher une / des bibliothèques universitaires en fonction d'une spécialité (maths, biologie, histoire, ...) d'ouvrage 

| Intention | Ubib train | Ubib test |
| --- | --- | --- |
| bot challenge | 62 | 9 |
| get timetable of library | 2812 | 162 |
| out of scope | 1433 | 356 |
| search library fields | 4925 | 141 |
| greet | 11 | 3 | 
| search library address | 2637 | 256 | 
| search libraries from field | 522 | 70 |

### Etiquettes de concepts

Signification :
- library : Nom d'une bibliothèque universitaire
- field : Spécialité (maths, biologie, histoire, ...)
- month : Mois (dans une date)
- period_relative : Période temporelle définie (ex : les vacances de Noël)
- day_of_month : Jour du mois dans une date (ex : 15 dans "le 15 novembre")
- day_relative : Date se rapportant à un jour et définie par rapport au jour courant (ex : aujourd'hui, demain)
- week_relative : Date se rapportant à une semaine et définie par rapport à celle courante (ex : dans une semaine)
- day_of_the_week : Jour de la semaine (ex: lundi, mardi)
- date_relative : Terme permettant de situé une date (ex : prochain ou passé dans "lundi prochain" ou "le mois passé")

Certains de ces concepts sont absents du corpus dibiso, mais étaient présents dans le corpus ubib.

| Concept | Ubib train | Ubib test |
| --- | --- | --- |
| library | 11920 | 609 |
| field | 8353 | 515 |
| month | 67 | 30 |
| period_relative | 545 | 76 | 
| day_of_month | 1079 | 9 |
| day_relative | 287 | 54 |
| week_relative | 61 | 9
| day_of_the_week | 576 | 43 |
| date_relative | 43 | 13 |

