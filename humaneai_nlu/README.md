# Humane.AI

Corpus anglais et français Humane.AI (tourisme) annoté en intentions et concepts (tâche de détection d'intention et de slot filling).
Ensemble de 4 autres corpus : MEDIA (FR), SNIPS (EN), DSTC2 (EN) et DSTC3 (EN).
Créé pour le projet Humane.AI, multilingue.
Les 4 corpus ont été uniformisés dans leurs intentions et concepts.

## Formats
- tsv

## Source
Travail issu du projet Humane.AI

## Autre

### Dossiers

- tsv : Version finale du corpus répartis en 3 lots.

### Nombres d'énoncés par lot


| Humane.AI | # énoncés |
| --- | --- |
| humaneai_train.tsv | 61199 |
| humaneai_dev.tsv | 3827 |
| humaneai_test.tsv | 11475 |
| Total | 76501 |


### Distribution des différentes intentions

| Intention | train | dev | test |
| --- | --- | --- | --- |
| cancel | 53 | 8 | 14 | 75 |
| confirm | 5717 | 366 | 1181 | 7264 |
| discourse_markers | 539 | 43 | 138 | 720 |
| get_weather | 1659 | 105 | 336 | 2100 |
| greet | 5525 | 321 | 1022 | 6868 |
| inform | 22810 | 1456 | 4311 | 28577 |
| negate | 1886 | 142 | 387 | 2415 |
| non_sense | 3470 | 245 | 639 | 4354 |
| out_of_scope | 6622 | 422 | 1208 | 8252 |
| reqalts | 1651 | 101 | 282 | 2034 |
| request | 12484 | 709 | 2181 | 15374 |
| thankyou | 1343 | 90 | 235 | 1668 |


### Distribution des différents concepts

| Concept | train | dev | test |
| --- | --- | --- | --- |
| business-availability | 114 | 4 | 28 | 146 |
| business-brands | 463 | 20 | 73 | 556 |
| business-carpark | 235 | 11 | 24 | 270 |
| business-comfort | 46 | 1 | 9 | 56 |
| business-equipment | 238 | 22 | 77 | 337 |
| business-facility-or-activity | 1221 | 80 | 230 | 1531 |
| business-name | 875 | 63 | 153 | 1091 |
| business-others | 194 | 19 | 51 | 264 |
| business-ratingsystem | 1291 | 69 | 210 | 1570 |
| business-services | 1795 | 121 | 322 | 2238 |
| business-subtype | 3179 | 188 | 556 | 3923 |
| business-type | 11489 | 716 | 2152 | 14357 |
| command-ask | 6797 | 400 | 1162 | 8359 |
| command-book | 2881 | 179 | 516 | 3576 |
| command-cancel | 60 | 8 | 12 | 80 |
| command-check | 63 | 2 | 15 | 80 |
| command-choice | 209 | 15 | 50 | 274 |
| command-confirm | 175 | 8 | 40 | 223 |
| command-modify | 72 | 5 | 10 | 87 |
| command-other | 3382 | 210 | 637 | 4229 |
| command-repeat | 181 | 5 | 23 | 209 |
| command-seek | 1932 | 117 | 325 | 2374 |
| cultural-adjective | 5057 | 364 | 993 | 6414 |
| cultural-event | 320 | 14 | 61 | 395 |
| cultural-food | 1954 | 120 | 370 | 2444 |
| cultural-music-genre | 2088 | 135 | 389 | 2612 |
| location-area | 2315 | 133 | 438 | 2886 |
| location-cardinal | 3111 | 183 | 571 | 3865 |
| location-city | 2875 | 196 | 549 | 3620 |
| location-continent | 4 | 1 | 1 | 6 |
| location-country | 750 | 45 | 147 | 942 |
| location-description | 454 | 28 | 95 | 577 |
| location-poi | 1086 | 68 | 229 | 1383 |
| location-scale | 91 | 2 | 24 | 117 |
| location-state-county | 1342 | 86 | 271 | 1699 |
| location-street | 39 | 4 | 11 | 54 |
| logical-relation-word | 5988 | 347 | 1058 | 7393 |
| named-object | 4141 | 272 | 759 | 5172 |
| number-digit | 12174 | 744 | 2251 | 15169 |
| number-nondigit | 215 | 9 | 44 | 268 |
| number-precision | 25 | 2 | 5 | 32 |
| number-ranking | 1149 | 90 | 225 | 1464 |
| number-tuple | 1289 | 61 | 189 | 1539 |
| payment-currency | 1017 | 55 | 173 | 1245 |
| payment-method | 3 | 1 | 1 | 5 |
| people-description | 1188 | 73 | 241 | 1502 |
| people-designation | 16707 | 1001 | 3073 | 20781 |
| people-famous | 1715 | 102 | 346 | 2163 |
| people-name | 510 | 35 | 62 | 607 |
| people-relations | 99 | 5 | 13 | 117 |
| people-title | 179 | 9 | 21 | 209 |
| relative-communication | 3753 | 206 | 647 | 4606 |
| relative-date | 879 | 45 | 172 | 1096 |
| relative-different | 353 | 30 | 71 | 454 |[Source github](https://github.com/matthen/dstc)
| relative-each | 43 | 3 | 13 | 59 |
| relative-entertainment | 3355 | 203 | 569 | 4127 |
| relative-foodtype | 1020 | 58 | 159 | 1237 |
| relative-location | 7352 | 426 | 1257 | 9035 |
| relative-music | 1525 | 112 | 292 | 1929 |
| relative-name | 90 | 3 | 9 | 102 |
| relative-other | 1127 | 55 | 206 | 1388 |
| relative-price | 6174 | 340 | 1120 | 7634 |
| relative-quantity | 893 | 59 | 171 | 1123 |
| relative-ranking | 277 | 20 | 46 | 343 |
| relative-same | 111 | 8 | 26 | 145 |
| relative-spatiotemporal | 9698 | 625 | 1910 | 12233 |
| relative-this | 1469 | 85 | 264 | 1818 |
| response-affirm | 7635 | 507 | 1509 | 9651 |
| response-dontcare | 3062 | 212 | 622 | 3896 |
| response-negate | 3023 | 216 | 563 | 3802 |
| response-other | 136 | 10 | 18 | 164 |
| temperature-description | 399 | 31 | 85 | 515 |
| time-day | 60 | 4 | 18 | 82 |
| time-decade | 307 | 24 | 64 | 395 |
| time-event | 43 | 2 | 12 | 57 |
| time-fromcurrent | 395 | 28 | 78 | 501 |
| time-fulldate | 39 | 1 | 14 | 54 |
| time-holidays | 117 | 12 | 21 | 150 |
| time-hour | 570 | 26 | 130 | 726 |
| time-month | 2115 | 127 | 405 | 2647 |
| time-period | 1866 | 140 | 385 | 2391 |
| time-season | 41 | 1 | 7 | 49 |
| time-year | 489 | 43 | 83 | 615 |
| weather-description | 380 | 26 | 75 | 481 |


### Plus de détails

[Document overleaf](https://fr.overleaf.com/read/qysrpygvwrkd)